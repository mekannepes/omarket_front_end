import React, {Component} from "react";
import { TextField } from '@material-ui/core';
import PropTypes from 'prop-types'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import TableCategory from './TableCategory'
import axios  from 'axios';
import { getBaseURL } from './../helpers/url';
const styles = makeStyles(theme => ({
    container: {
      display: 'inline',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 400,
    },
    cardAvatar:{
        width: '18rem',
    },
  }));

const authToken = localStorage.getItem("authToken")

class Profile extends Component{

    constructor(props){
        super(props);

        this.state = {
            store:{
                address: "",
                categories:  [],
                count_goods: 0,
                email: "",
                id: "",
                image_address: "",
                name: "",
                password: "",
                phone: "",
                shopkeeper: "",
                shopkeeper_phone: "",
                sum: 0,
                sum_with_discount: 0,
            }
        }
    }
    componentDidMount(){

        axios.get(getBaseURL()+"/store",{
            headers:{
                'Authorization': "Bearer " + authToken,
              },
        }).then(resp => {
            const store = resp.data
            this.setState({store})
//            this.setState((prevState) => ({ store: { ...prevState.store, ["categories"]: store.categories } }));
        }).catch(err => {
            console.log(err)
        })
    }

    render(){
        const { classes } = this.props;
        const {store} = this.state
        return(
            <div className="content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header card-header-primary">
                                    <h4 className="card-title">Edit Profile</h4>
                                    <p className="card-category">Complete your profile</p>
                                </div>
                                <div className="card-body">   
                                
                                    <form  noValidate autoComplete="off">
                                        <div className={classes.container}>
                                            <div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <TextField
                                                            disabled
                                                            id="outlined-required"
                                                            label="Email"                                                         
                                                            value={store.email}
                                                            className={classes.textField}
                                                            margin="normal"
                                                            variant="outlined"
                                                            helperText=" "
                                                            />
                                                        </div>
                                                        <TextField
                                                            disabled
                                                            id="outlined-required"
                                                            label="Parol"
                                                            value={store.password}
                                                            className={classes.textField}
                                                            margin="normal"
                                                            variant="outlined"
                                                            helperText=" "
                                                            />
                                                    <div className="col-md-6">
                                                        <TextField
                                                            id="outlined-required"
                                                            label="Magazynyn ady"
                                                            value={store.name}
                                                            className={classes.textField}
                                                            margin="normal"
                                                            variant="outlined"
                                                            helperText=" "
                                                            />
                                                        </div>
                                                    <TextField
                                                        id="outlined-required"
                                                        label="Magazynyn tel nomeri"
                                                        value={store.phone}
                                                        className={classes.textField}
                                                        margin="normal"
                                                        variant="outlined"
                                                        helperText=" "
                                                        />
                                                    <TextField
                                                        id="outlined-full-width"
                                                        label="Magazynyn adresi"
                                                        placeholder="Magazynyn adresi"
                                                        value={store.address}
                                                        fullWidth
                                                        margin="dense"
                                                        variant="outlined"
                                                        helperText=" "
                                                        style={{ margin: 15 }}
                                                        InputLabelProps={{
                                                            shrink: true,
                                                        }}
                                                        />
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <TextField
                                                        id="outlined-required"
                                                        label="Magazynyn eyesinin ady"
                                                        value={store.shopkeeper}
                                                        className={classes.textField}
                                                        margin="normal"
                                                        variant="outlined"
                                                        helperText=" "
                                                        />
                                                    </div>
                                                    <TextField
                                                        id="outlined-required"
                                                        label="Magazynyn eyesinin tel nomeri"
                                                        value={store.shopkeeper_phone}
                                                        className={classes.textField}
                                                        margin="normal"
                                                        variant="outlined"
                                                        helperText=" "
                                                        />
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div className={classes.container}>
                                            <TableCategory 
                                            categories={store.categories}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card ">
                                <div className="card-avatar" >
                                    <img className="card-img-top" src="/logo512.png" alt="Card image cap"/>
                                </div>
                                <div className="card-body">
                                    <h6 className="card-category text-gray">CEO / Co-Founder</h6>
                                    <h4 className="card-title">Alec Thompson</h4>
                                    <p className="card-description">
                                        Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Profile.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(Profile);