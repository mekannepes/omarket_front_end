import React, {Component} from "react";
import Sidebar from "./Sidebar";
import MainPanel from "./MainPanel";

class Main extends Component{
    render(){
        return(
            <div>
                <Sidebar/>
                <MainPanel/>
            </div>
        )
    }
}

export default Main;