import React from 'react';
import MaterialTable from 'material-table';
import  axios  from 'axios';

const authToken = localStorage.getItem("authToken")

const categories = [
  {id:1,name:"erkek"},
  {id:2,name:"elektronika"},
  {id:3,name:"Sport esikleri we geyimler"},
  {id:4,name:"Sport esikleri"},
]



class TableCategory extends React.Component {
  constructor(props){
    super(props);
    var obj = categories.reduce(function(acc, cur, i) {
      acc[cur.id] = cur.name;
    
      return acc;
    }, {});
    console.log(this.props.categories)
    this.state = {
        columns: [
          {
            title: 'Category',
            field: 'category',
            lookup: obj,                       
          },
        ],
        data: [
          { category: 1, },
          { category: 2, },
        ],
    }
  };

  render(){
    return (
        <MaterialTable
          title="Kategoriyalar"
          columns={this.state.columns}
          data={this.state.data}
          editable={{
            onRowAdd: newData =>

            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                this.setState(prevState => {
                  const data = [...prevState.data];
                  
                  axios.put("http://localhost:8080/api/category",
                  {
                    category:newData.category
                  },
                    {
                      headers:{
                        'Content-Type' : 'application/json',
                        'Authorization': "Bearer " + authToken,
                      },
                  }).then(res => {
                    console.log(res)
        //                  var response = res.data;
                    data.push(newData);
      
                  }).catch(res => {
                    console.log(res)
                  })
                  return { ...prevState, data };
                });
              }, 600);
            }),
            onRowDelete: oldData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  this.setState(prevState => {
                    const data = [...prevState.data];
                    data.splice(data.indexOf(oldData), 1);
                    return { ...prevState, data };
                  });
                }, 600);
              }),
          }}
        />
      )
  }
}

export default TableCategory;