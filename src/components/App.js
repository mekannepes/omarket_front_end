import React, {Component} from "react";
import Main from "./Main";
import {BrowserRouter, Switch,Route} from "react-router-dom"
import PrivateRoute from "./PrivateRoute";
import Login from './Login';
class App extends Component{
    render(){
        return(
            <div className="wrapper ">
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Login}/>
                        <PrivateRoute path="/admin" component={Main} />
                    </Switch>
                </BrowserRouter>
            </div>
        )
    }
}

export default App;