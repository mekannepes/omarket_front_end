import React, {Component} from "react";
import axios from "axios";
import { getJwt } from './../helpers/jwt';
import "../style/Login.css"

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  );

const formValid = formErrors =>{
    var valid = true

    Object.values(formErrors).forEach(val => {
        val.length > 0  &&  (valid=false)
    })

    return valid
};

class Login extends Component{
    constructor(props){
        super(props)

        this.state = {
            email:"",
            password:"",
            formErrors:{
                errorMessage:"",
                email:"",
                password:""
            }
        }
//        this.change = this.change.bind(this);
    }
    componentWillMount(){
        var jwt = getJwt()
        if(jwt){
            this.props.history.push('/admin/dashboard');
        }
    }


    handleSubmit = event => {
        event.preventDefault();
        if(formValid(this.state.formErrors)){
            axios.post("http://localhost:8080/login",{
                headers: {
                    'Content-Type' : 'application/json'
                }, 
                email:this.state.email,
                password:this.state.password
            })
            .then(res => {
                var authToken = res.data
                localStorage.setItem("authToken",authToken.token);
                this.props.history.push('/admin/dashboard');
            }).catch(rea =>{
                let formErrorsCopy = JSON.parse(JSON.stringify(this.state.formErrors))
                formErrorsCopy.errorMessage = "Email yada parol yalnysh girizilen"
                this.setState(function(state, props){
                    return {    
                        formErrors:formErrorsCopy
                    }
                });
            })
        }
    };

    handleChange =event => {
        event.preventDefault();

        const {name,value} = event.target

        let formErrors = { ...this.state.formErrors };

        switch(name){
            case "email":
                formErrors.email = 
                emailRegex.test(value)  &&  value.length > 0
                ? ""
                : "Email adres yalnysh yazylan"
                break;
            case "password":
                formErrors.password = 
                value.length < 6
                ? "Azyndan 6 belgili parol yaz"
                : ""
                break;
            default:
                break;
        }
        if(formErrors.errorMessage.length>0){
            formErrors.errorMessage=""
        }
        this.setState({formErrors,[name]:value})
    };

    render(){
        const {formErrors} =this.state
        return(
            <div className="wrapperSign">
                <div className="form-wrapperSign">
                    <h1>Girmek</h1>
                    {formErrors.errorMessage.length > 0 && (
                        <span className="errorMessage">{formErrors.errorMessage}</span>
                    )}
                    <form onSubmit={this.handleSubmit} noValidate>                        
                        <div className="email">
                            <label htmlFor="email">Email</label>
                            <input
                                className={formErrors.email.length > 0 ? "error" : null}
                                placeholder="Email"
                                type="email"
                                name="email"
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.email.length > 0 && (
                                <span className="errorMessage">{formErrors.email}</span>
                            )}
                        </div>
                        <div className="password">
                            <label htmlFor="password">Parol</label>
                            <input
                                className={formErrors.password.length > 0 ? "error" : null}
                                placeholder="Password"
                                type="password"
                                name="password"
                                noValidate
                                onChange={this.handleChange}
                            />
                            {formErrors.password.length > 0 && (
                                <span className="errorMessage">{formErrors.password}</span>
                            )}
                            </div>
                            <div className="signUpAccaunt">
                            <button type="submit">Gir</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;