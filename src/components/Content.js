import React, {Component} from "react";
import {Switch, Route} from "react-router-dom"
import Dashboard from "./Dashboard"
import Profile from "./Profile"
class Content extends Component{
    render(){
        return(
            <Switch>
                <Route exact path="/admin" component={Dashboard}/>
                <Route path="/admin/dashboard" component={Dashboard}/>
                <Route path="/admin/profile" component={Profile}/>
            </Switch>
        )
    }
}

export default Content;